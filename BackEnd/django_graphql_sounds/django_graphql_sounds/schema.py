import graphene

import django_graphql_sounds.sounds.schema 


class Query(django_graphql_sounds.sounds.schema.Query, graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

schema = graphene.Schema(query=Query)