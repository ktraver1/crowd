from django.contrib import admin
from django_graphql_sounds.sounds.models import Sound,Trigger,User
# Register your models here.

admin.site.register(Sound)
admin.site.register(Trigger)
admin.site.register(User)