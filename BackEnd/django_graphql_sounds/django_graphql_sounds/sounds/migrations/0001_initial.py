# Generated by Django 2.2.5 on 2019-10-02 00:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Trigger',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('trigger', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='Sound',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(max_length=256)),
                ('name', models.CharField(max_length=128)),
                ('description', models.CharField(max_length=128)),
                ('triggers', models.ManyToManyField(to='sounds.Trigger')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sounds.User')),
            ],
            options={
                'ordering': ('user', 'name'),
            },
        ),
    ]
