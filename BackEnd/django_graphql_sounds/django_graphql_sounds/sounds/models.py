from django.db import models

class Trigger(models.Model):
    """
        Triggers that are asscocaited with sound
    """
    trigger = models.CharField(max_length=256)

    def __str__(self):
        return self.trigger

class User(models.Model):
    """
        User table
    """
    username = models.CharField(max_length=32,unique=True)

    def __str__(self):
        return self.username

# Sounds table
class Sound(models.Model):
    """
        Table containg path and owner of sound asset stored in S3
    """

    # feilds used in database
    # user that owns the sounder 
    # if the user is deleted then delete the sounds(cascade)
    user        = models.ForeignKey(User, on_delete=models.CASCADE)
    # CDN url of the sound 
    url         = models.CharField(max_length=256)
    # name of the sound asset
    name        = models.CharField(max_length=128)
    # breif description of the sound
    description = models.CharField(max_length=128)
    # list of triggers for the sound
    triggers    = models.ManyToManyField(Trigger)

    def __str__(self):
        return self.name
    class Meta:
        ordering = ('user','name',)

