import graphene
from graphene_django.types import DjangoObjectType
from django_graphql_sounds.sounds.models import Sound, Trigger, User

class SoundType(DjangoObjectType):
    class Meta:
        model = Sound
class TriggerType(DjangoObjectType):
    class Meta:
        model = Trigger
class UserType(DjangoObjectType):
    class Meta:
        model = User

class Query(object):
    all_triggers = graphene.List(TriggerType)
    trigger = graphene.Field(TriggerType, id=graphene.Int())

    all_sounds = graphene.List(SoundType)
    sound      = graphene.Field(SoundType,
                              id=graphene.Int(),
                              user=graphene.String())
    username       = graphene.Field(UserType, id=graphene.Int(),username=graphene.String())

    def resolve_all_sounds(self, info, **kwargs):
        return Sound.objects.all()

    def resolve_all_triggers(self, info, **kwargs):
        return Trigger.objects.all()

    def resolve_sound(self, info, **kwargs):
        id = kwargs.get('id')
        user = kwargs.get('user')

        if id is not None:
            return Sound.objects.get(pk=id)

        if user is not None:
            return Sound.objects.get(user=user)
        return None
    def resolve_username(self, info, **kwargs):
        id = kwargs.get('id')
        username = kwargs.get('username')

        if id is not None:
            return User.objects.get(pk=id)

        if username is not None:
            return User.objects.get(username=username)
        return None