# Imports
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
basedir = os.path.abspath(os.path.dirname(__file__))
import graphene
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from flask_graphql import GraphQLView


# app initialization
app = Flask(__name__)
app.debug = True

# Configs for database 
# connect to aws rds
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" +    os.path.join(basedir, "data.sqlite")
username = os.environ['username']
password = os.environ['password']
rds_url  = os.environ['rds_url']
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://{}:{}@{}".format(username,password,rds_url)
app.config["SQLALCHEMY_COMMIT_ON_TEARDOWN"] = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

# Modules
db = SQLAlchemy(app)

# Models
trigger_relationship = db.Table("trigger_relationship",
    db.Column("trigger_id",db.Integer,db.ForeignKey("triggers.trigger_id")),
    db.Column("sound_id",db.Integer,db.ForeignKey("sounds.sound_id")),
    db.PrimaryKeyConstraint('trigger_id', 'sound_id')
)

class Trigger(db.Model):
    __tablename__ = "triggers"
    trigger_id = db.Column(db.Integer, primary_key=True)
    trigger = db.Column(db.String(256), index=True)

    def __repr__(self):
            return "<Trigger %r>" % self.trigger


class User(db.Model):
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(256), index=True, unique=True)
    sounds = db.relationship("Sound", backref="user")
    
    def __repr__(self):
        return "<User %r>" % self.username

class Sound(db.Model):
    __tablename__ = "sounds"
    sound_id = db.Column(db.Integer, primary_key=True)
    # user that owns the sounder
    user_id = db.Column(db.Integer, db.ForeignKey("users.user_id"))
    # CDN url of the sound 
    url = db.Column(db.String(256), index=True)
    # name of the sound asset
    name = db.Column(db.String(256), index=True)
    # breif description of the sound
    description = db.Column(db.Text)
    # list of triggers for the sound
    triggers = db.relationship("Trigger",secondary=trigger_relationship,backref="sounds")

    def __repr__(self):
        return "<Sound %r>" % self.name



# Schema Objects
class SoundObject(SQLAlchemyObjectType):
    class Meta:
        model = Sound
        interfaces = (graphene.relay.Node, )
class UserObject(SQLAlchemyObjectType):
   class Meta:
       model = User
       interfaces = (graphene.relay.Node, )
class TriggerObject(SQLAlchemyObjectType):
   class Meta:
       model = Trigger
       interfaces = (graphene.relay.Node, )
class Query(graphene.ObjectType):
    node = graphene.relay.Node.Field()
    all_sounds = SQLAlchemyConnectionField(SoundObject)
    all_users = SQLAlchemyConnectionField(UserObject)
    all_triggers = SQLAlchemyConnectionField(TriggerObject)

class CreateSound(graphene.Mutation):
    class Arguments:
        url = graphene.String(required=True)
        name = graphene.String(required=True) 
        username = graphene.String(required=True)
        description = graphene.String(required=True)
    sound = graphene.Field(lambda: SoundObject)
    def mutate(self, info, url, name, username,description):
        user = User.query.filter_by(username=username).first()
        sound = Sound(url=url, name=name,description=description)
        if user is not None:
            sound.user = user
        db.session.add(sound)
        db.session.commit()
        return CreateSound(sound=sound)
class Mutation(graphene.ObjectType):
    create_sound = CreateSound.Field()
schema = graphene.Schema(query=Query, mutation=Mutation)


# Routes
# Creates route /graphql to be called
app.add_url_rule(
    "/graphql",
    view_func=GraphQLView.as_view(
        "graphql",
        schema=schema,
        graphiql=True # for having the GraphiQL interface
    )
)

@app.route("/")
def index():
    return "<p>Welcome to the Crowd App</p>"

if __name__ == "__main__":
     app.run()