from app import db, User, Sound, Trigger

class createDB:
    def __init__(self):
        db.create_all()
        default = User(username='default')
        sound = Sound()
        boom = Trigger()
        boom.trigger = "boom"
        head = Trigger()
        head.trigger = "boom headshot"
        sound.url = "Url/to/boom/head/shot"
        sound.name = "Boom Head Shot"
        sound.description = "Boom Head Shot"
        sound.user = default
        db.session.add(sound)
        db.session.add(default)
        db.session.add(boom)
        db.session.add(head)
        db.session.commit()
        sound.triggers.append(boom)
        sound.triggers.append(head)
        db.session.commit()
        print(User.query.all())
        print(Sound.query.all())
        print(Trigger.query.all())
        print("DB created.")

if __name__=="__main__":
    createDB()